/**
 * import package
 */
'use strict';
/**
 * class fractal
 */
class Fractal {
  /**
   * construct function
   */
  constructor() {
    this.canvas = document.getElementById('ctxCanvas');
    this.context = this.canvas.getContext('2d');
    this.imageW = this.canvas.width;
    this.imageH = this.canvas.height;
    this.imageData = this.context.createImageData(this.imageW, this.imageH);
    this.offSetX = - this.imageW / 2;
    this.offSetY = - this.imageH / 2;
    this.panX = -100;
    this.panY = 0;
    this.zoom = 150;
    this.palette = [];
    this.maxIterations = 250;
  }

  /**
   * funcion principal
   */
  main() {

    this.canvas.addEventListener("mousedown", this.onMouseDown);
    this.generatePalette();
    this.generateImage();
    this.generateLoopMain(0);
  }

  /**
   * generar el loop de convolucion del fractal
   */
  generateLoopMain(ct) {
    window.requestAnimationFrame(this.generateLoopMain);
    this.context.putImageData(this.imageData, 0, 0);
  }

  /**
   * generar paleta de color
   */
  generatePalette() {
    //calcular el gradiente
    let rOffSet = 24, gOffSet = 16, bOffSet = 0;
    for (let i = 0; i < 256; i++) {
      this.palette[i] = {
        r: rOffSet,
        g: gOffSet,
        b: bOffSet
      };

      if (i < 64) {
        rOffSet += 3;
      } else if (i < 128) {
        gOffSet += 3;
      } else if (i <192) {
        bOffSet += 3;
      }
    }
  }

  /**
   * generar imagen del fractal
   */
  generateImage() {
    for (let y=0; y<this.imageH; y++) {
      for (let x=0; x<this.imageW; x++) {
        this.iterate(x, y, this.maxIterations);
      }
    }
  }

  /**
   * iteracciones del fractal
   */
  iterate(x, y, maxiterations) {
    let x0 = (x + this.offSetX + this.panX) / this.zoom;
    let y0 = (y + this.offSetY + this.panY) / this.zoom;

    // Iteration variables
    let a = 0;
    let b = 0;
    let rx = 0;
    let ry = 0;

    // Iterate
    let iterations = 0;
    while (iterations < maxiterations && (rx * rx + ry * ry <= 4)) {
      rx = a * a - b * b + x0;
      ry = 2 * a * b + y0;
      // Next iteration
      a = rx;
      b = ry;
      iterations++;
    }

    // Get palette color based on the number of iterations
    let color;
    if (iterations == maxiterations) {
      color = { r:0, g:0, b:0}; // Black
    } else {
      let index = Math.floor((iterations / (maxiterations - 1)) * 255);
      color = this.palette[index];
    }

    // Apply the color
    let pixelindex = (y * this.imageW + x) * 4;
    this.imageData.data[pixelindex] = color.r;
    this.imageData.data[pixelindex + 1] = color.g;
    this.imageData.data[pixelindex + 2] = color.b;
    this.imageData.data[pixelindex + 3] = 255;
  }

  /**
   * generar zoom imagen
   */
  zoomFractal(x, y, factor, zooomin) {
    if (zoomin) {
      // Zoom in
      zoom *= factor;
      this.panX = factor * (x + this.offSetX + this.panX);
      this.pany = factor * (y + this.offSetY + this.pany);
    } else {
      // Zoom out
      zoom /= factor;
      this.panX = (x + this.offSetX + this.panX) / factor;
      this.panY = (y + this.offSetY + this.panY) / factor;
    }
  }

  /**
   * evento del mouse
   */
  onMouseDown(e) {
    let pos = this.getMousePos(this.canvas, e);
    // Zoom out with Control
    let zoomin = true;
    if (e.ctrlKey) {
      zoomin = false;
    }

    // Pan with Shift
    let zoomfactor = 2;
    if (e.shiftKey) {
      zoomfactor = 1;
    }

    // Zoom the fractal at the mouse position
    this.zoomFractal(pos.x, pos.y, zoomfactor, zoomin);

    // Generate a new image
    this.generateImage();
  }

  /**
   * Funcion para obtener la posicion del mouse
   */
  getMousePos(canvas, e) {
    let rect = canvas.getBoundingClientRect();
    return {
      x: Math.round((e.clientX - rect.left) / (rect.right - rect.left) * canvas.width),
      y: Math.round((e.clientY - rect.top) / (rect.bottom - rect.top) * canvas.height)
    };
  }
}
