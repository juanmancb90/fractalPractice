(function () {
  'use strict';
  try {
    window.onload = function() {
      var context = new Fractal;
      console.log(context);
      context.main();
    };
  } catch (e) {
    console.info(e.toString());
  }
})();
